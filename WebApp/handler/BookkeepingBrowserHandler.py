from WebAppDIRAC.Lib.WebHandler import WebHandler, WErr, WOK, asyncGen
from LHCbDIRAC.BookkeepingSystem.Client.LHCB_BKKDBClient  import LHCB_BKKDBClient
from DIRAC.FrameworkSystem.Client.UserProfileClient import UserProfileClient
from DIRAC.Core.Utilities import Time
import json
import tornado
import sys
import datetime

class BookkeepingBrowserHandler( WebHandler ):

  AUTH_PROPS = "authenticated"

  def index( self ):
    pass

  @asyncGen
  def web_getNodes( self ):

    path, type, tree, dataQuality = self.__parseRequest()

    node = ''
    if 'node' in self.request.arguments:
      node = self.request.arguments['node'][-1]

    bk = LHCB_BKKDBClient( web = True )
    
    yield self.threadTask( bk.setFileTypes, [] )
    
    bk.setAdvancedQueries( type )
    bk.setParameter( tree )
    bk.setDataQualities( dataQuality )

    retVal = yield self.threadTask( bk.list, node )

    nodes = []
    if len( retVal ) > 0:
      for i in retVal:
        node = {}
        node['text'] = i['name']
        node['fullpath'] = i['fullpath']
        node['id'] = i['fullpath']
        node['selection'] = i['selection'] if 'selection' in i else ''
        node['method'] = i['method'] if 'method' in i else ''
        node['cls'] = "folder" if i['expandable'] else 'file'
        if 'level' in i and i['level'] == 'Event types':
          node['text'] = "%s (%s)" % (i['name'], i['Description'])
        if 'level' in i and i['level'] == 'FileTypes':
          node['leaf'] = True
        else:
          node['leaf'] = False if i['expandable'] else True
        if 'level' in i:
          node['level'] = i['level']
          node['qtip'] = i['level']
        nodes += [node]

    result = tornado.escape.json_encode( nodes )
    self.finish( result )

  @asyncGen
  def web_getdataquality( self ):
    bk = LHCB_BKKDBClient( web = True )
    result = yield self.threadTask( bk.getAvailableDataQuality )
    if result["OK"]:
      ret = []
      for i in result['Value']:
        checked = True if i == "OK" else False
        ret += [{'name':i, 'value':checked}]
      self.finish( {"success":"true", "result":ret} )
    else:
      self.finish( {"result":[], "error":result['Message']} )

  @asyncGen
  def web_getFiles( self ):

    path, type, tree, dataQuality = self.__parseRequest()

    bk = LHCB_BKKDBClient( web = True )

    bk.setAdvancedQueries( type )
    bk.setParameter( tree )
    bk.setDataQualities( dataQuality )

    retVal = yield self.threadTask( bk.getLimitedFiles, {'fullpath':path}, {'total':'0'}, self.pageNumber, self.numberOfJobs + self.pageNumber )

    nbrecords = retVal['TotalRecords']
    if nbrecords > 0:
      params = retVal['ParameterNames']
      records = []
      for i in retVal['Records']:
        k = [j if j != None and j != 'None' else '-' for j in i]
        records += [dict( zip( params, k ) )]
      extras = {}
      if "Extras" in retVal:
        extras = retVal["Extras"]
        extras["GlobalStatistics"]["Number of Files"] = nbrecords
        size = self.__bytestr( extras["GlobalStatistics"]["Files Size"] )
        extras["GlobalStatistics"]["Files Size"] = size

      timestamp = Time.dateTime().strftime( "%Y-%m-%d %H:%M [UTC]" )
      data = {"success":"true", "result":records, "date":timestamp, "total":nbrecords, "ExtraParameters":extras}
    else:
      data = {"success":"false", "result":[], "error": "Nothing to display!"}


    self.finish( data )


  ################################################################################
  @staticmethod
  def __bytestr( size, precision = 1 ):
    """Return a string representing the greek/metric suffix of a size"""
    abbrevs = [( 1 << 50L, ' PB' ), ( 1 << 40L, ' TB' ), ( 1 << 30L, ' GB' ), ( 1 << 20L, ' MB' ), ( 1 << 10L, ' kB' ), ( 1, ' bytes' )]
    if size is None:
      return '0 bytes'
    if size == 1:
      return '1 byte'
    for factor, suffix in abbrevs:
      if size >= factor:
        break
    float_string_split = `size / float( factor )`.split( '.' )
    integer_part = float_string_split[0]
    decimal_part = float_string_split[1]
    if int( decimal_part[0:precision] ):
      float_string = '.'.join( [integer_part, decimal_part[0:precision]] )
    else:
      float_string = integer_part
    return float_string + suffix

  def __parseRequest( self ):
    path = ''
    if 'fullpath' in self.request.arguments:
      path = self.request.arguments['fullpath'][-1]

    type = False
    if 'type' in self.request.arguments:
      type = True if self.request.arguments['type'][-1] == 'adv' else False

    tree = 'Configuration'
    if 'tree' in self.request.arguments:
      tree = self.request.arguments['tree'][-1]

    dataQuality = None
    if 'dataQuality' in self.request.arguments:
      dataQuality = dict( json.loads( self.request.arguments[ 'dataQuality' ][-1] ) )
      if len( dataQuality ) == 0:
        dataQuality = {'OK':True}

    if "limit" in self.request.arguments:
      self.numberOfJobs = int( self.request.arguments["limit"][-1] )
      if "start" in self.request.arguments:
        self.pageNumber = int( self.request.arguments["start"][-1] )
      else:
        self.pageNumber = 0
    else:
      self.numberOfJobs = 25
      self.pageNumber = 0
    return path, type, tree, dataQuality

  @asyncGen
  def web_getStatistics( self ):

    path, type, tree, dataQuality = self.__parseRequest()

    bk = LHCB_BKKDBClient( web = True )

    bk.setAdvancedQueries( type )
    bk.setParameter( tree )
    bk.setDataQualities( dataQuality )

    retVal = yield self.threadTask( bk.getLimitedInformations, self.pageNumber, self.numberOfJobs + self.pageNumber, path )
    if retVal['OK']:
      value = {}
      value['nbfiles'] = retVal['Value']['Number of files']
      value['nbevents'] = self.__niceNumbers( retVal['Value']['Number of Events'] )
      value['fsize'] = self.__bytestr( retVal['Value']['Files Size'] )
      data = {"success":"true", "result":value}
    else:
      data = {"success":"false", "result":[], "error": retVal['Message']}
    # data = {"success":"true","result":{'nbfiles':0,'nbevents':0,'fsize':0}}
    self.finish( data )

  @staticmethod
  def __niceNumbers( number ):
    strList = list( str( number ) )
    newList = [ strList[max( 0, i - 3 ):i] for i in range( len( strList ), 0, -3 ) ]
    newList.reverse()
    finalList = []
    for i in newList:
      finalList.append( str( ''.join( i ) ) )
    finalList = " ".join( map( str, finalList ) )
    return finalList

  @asyncGen
  def web_saveDataSet( self ):

    path, type, tree, dataQuality = self.__parseRequest()

    bk = LHCB_BKKDBClient( web = True )

    bk.setAdvancedQueries( type )
    bk.setParameter( tree )
    bk.setDataQualities( dataQuality )

    format = None
    if 'format' in self.request.arguments:
      format = self.request.arguments['format'][-1]

    fileName = None
    if 'fileName' in self.request.arguments:
      fileName = self.request.arguments['fileName'][-1]

    try:
      data = yield self.threadTask( bk.writePythonOrJobOptions, self.pageNumber, self.numberOfJobs + self.pageNumber, path, format )
    except:
      data = {"success":"false", "error":str( sys.exc_info()[1] )}

    self.set_header( 'Content-type', 'text/plain' )
    self.set_header( 'Content-Disposition', 'attachment; filename="%s' % fileName )
    self.set_header( 'Content-Length', len( data ) )
    self.set_header( 'Content-Transfer-Encoding', 'Binary' )
    self.set_header( 'Cache-Control', "no-cache, no-store, must-revalidate, max-age=0" )
    self.set_header( 'Pragma', "no-cache" )
    self.set_header( 'Expires', ( datetime.datetime.utcnow() - datetime.timedelta( minutes = -10 ) ).strftime( "%d %b %Y %H:%M:%S GMT" ) )
    self.finish( data )

  @asyncGen
  def web_getBookmarks( self ):
    upc = UserProfileClient( "Bookkeeping" )
    result = yield self.threadTask( upc.retrieveVar, "Bookmarks" )
    print 'cccc', result
    if result["OK"]:
      data = []
      for i in result['Value']:
        data += [{"name":i, "value":result['Value'][i]}]
      result = {"success":"true", "result":data}
    else:
      if result['Message'].find( "No data for" ) != -1:
        result = {"success":"true", "result":{}}
      else:
        result = {"success":"false", "error":result["Message"]}
    self.finish( result )

  @asyncGen
  def web_addBookmark( self ):
    print self.request.arguments
    title = ''
    if 'title' in self.request.arguments:
      title = self.request.arguments['title'][-1]

    path = ''
    if 'path' in self.request.arguments:
      path = self.request.arguments['path'][-1]

    upc = UserProfileClient( "Bookkeeping" )
    result = yield self.threadTask( upc.retrieveVar, "Bookmarks" )
    if result["OK"]:
      data = result["Value"]
    else:
      data = {}
    if title in data:
      result = {"success":"false", "error":"The bookmark with the title \"" + title + "\" is already exists"}
    else:
      data[title] = path

    result = yield self.threadTask( upc.storeVar, "Bookmarks", data )
    if result['OK']:
      result = {"success":"true", "result":"It successfully added to the bookmark!"}
    else:
      result = {"success":"false", "error":result["Message"]}
    self.finish( result )

  @asyncGen
  def web_deleteBookmark( self ):
    print self.request.arguments
    title = ''
    if 'title' in self.request.arguments:
      title = self.request.arguments['title'][-1]
      
    path = ''
    if 'path' in self.request.arguments:
      path = self.request.arguments['path'][-1]

    upc = UserProfileClient( "Bookkeeping" )
    result = yield self.threadTask( upc.retrieveVar, "Bookmarks" )

    if result["OK"]:
      data = result["Value"]
    else:
      data = {}

    if title in data:
      del data[title]
    else:
      result = {"success":"false", "error":"Can't delete not existing bookmark: \"" + title + "\""}

    result = yield self.threadTask( upc.storeVar, "Bookmarks", data )
    if result["OK"]:
      result = {"success":"true", "result":"It successfully deleted to the bookmark!"}
    else:
      result = {"success":"false", "error":result["Message"]}
    self.finish( result )



