from WebAppDIRAC.WebApp.handler.JobMonitorHandler import JobMonitorHandler
from WebAppDIRAC.Lib.WebHandler import asyncGen,WebHandler
from DIRAC.Core.DISET.RPCClient import RPCClient
from DIRAC.FrameworkSystem.Client.PlottingClient  import PlottingClient
from WebAppDIRAC.Lib.SessionData import SessionData
from DIRAC import gConfig, S_OK, S_ERROR, gLogger
import json

class LHCbJobMonitorHandler(JobMonitorHandler):

  AUTH_PROPS = "authenticated"

  def index(self):
    pass

  def web_standalone(self):
    self.render("JobMonitor/standalone.tpl", config_data = json.dumps(SessionData().getData()))

  def _request(self):
    req = super(LHCbJobMonitorHandler, self)._request()
    if "RunNumbers" in self.request.arguments:
      runs = list(json.loads(self.request.arguments[ 'RunNumbers' ][-1]))
      if len(runs) > 0:
        req['runNumber'] = runs
    
    return req
  
  

