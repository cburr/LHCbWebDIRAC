""" Web Handler for ProductionRequest service
"""

import json
import cPickle
import tornado
import datetime

from WebAppDIRAC.Lib.WebHandler import WebHandler, WErr, asyncGen

from DIRAC import S_ERROR, S_OK, gConfig, gLogger
from DIRAC.ConfigurationSystem.Client import PathFinder
from DIRAC.Core.DISET.RPCClient import RPCClient
from DIRAC.Core.Utilities import DictCache
from DIRAC.Core.Utilities import Time

from ProductionLib import SelectAndSort, PrTpl
from LHCbDIRAC.BookkeepingSystem.Client.LHCB_BKKDBClient import LHCB_BKKDBClient


class ProductionRequestManagerHandler(WebHandler):

  AUTH_PROPS = "authenticated"

  __dataCache = DictCache.DictCache()

  serviceFields = ['RequestID', 'HasSubrequest', 'ParentID', 'MasterID',
                   'RequestName', 'RequestType', 'RequestState',
                   'RequestPriority', 'RequestAuthor', 'RequestPDG', 'RequestWG',
                   'SimCondition', 'SimCondID',
                   'ProPath', 'ProID',
                   'EventType', 'NumberOfEvents', 'Comments', 'Description', 'Inform', 'IsModel',
                   'StartingDate', 'FinalizationDate', 'FastSimulationType', 'RetentionRate',
                   'bk', 'bkTotal', 'rqTotal', 'crTime', 'upTime']
  localFields = ['ID', '_is_leaf', '_parent', '_master',
                 'reqName', 'reqType', 'reqState',
                 'reqPrio', 'reqAuthor', 'reqPDG', 'reqWG',
                 'simDesc', 'simCondID',
                 'pDsc', 'pID',
                 'eventType', 'eventNumber', 'reqComment', 'reqDesc', 'reqInform', 'IsModel',
                 'StartingDate', 'FinalizationDate', 'FastSimulationType', 'RetentionRate',
                 'eventBK', 'eventBKTotal', 'EventNumberTotal',
                 'creationTime', 'lastUpdateTime']

  simcondFields = ['Generator', 'MagneticField', 'BeamEnergy',
                   'Luminosity', 'DetectorCond', 'BeamCond',
                   'configName', 'configVersion', 'condType',
                   'inProPass', 'inFileType', 'inProductionID',
                   'inDataQualityFlag', 'G4settings', 'inTCKs']

  proStepFields = ['Step', 'Name', 'Pass', 'App', 'Ver', 'Opt', 'OptF',
                   'DDDb', 'CDb', 'DQT', 'EP',
                   'Vis', 'Use', 'IFT', 'OFT', 'Html']

  extraFields = ['mcConfigVersion']

  bkSimCondFields = ['simCondID', 'simDesc', 'BeamCond',
                     'BeamEnergy', 'Generator',
                     'MagneticField', 'DetectorCond',
                     'Luminosity', 'G4settings']

  def __unescape(self, d):
    """ Unescape HTML code in parameters """
    for x in d:
      d[x] = unicode(d[x][-1])
      s = d[x].replace("&amp;", "&")
      while s != d[x]:
        d[x] = s
        s = d[x].replace("&amp;", "&")
      d[x] = d[x].replace("&lt;", "<")
      d[x] = d[x].replace("&gt;", ">")

  def __fromlocal(self, req):
    result = {}
    for x, y in zip(self.localFields[2:-5], self.serviceFields[2:-5]):
      if x in req:
        result[y] = req[x]

    SimCondDetail = {}
    for x in self.simcondFields:
      if x in req and str(req[x]) != '':
        SimCondDetail[x] = req[x]
    if SimCondDetail:
      result['SimCondDetail'] = cPickle.dumps(SimCondDetail)

    ProDetail = {}
    for x in req:
      if str(x)[0] == 'p' and str(x) != 'progress':
        if str(req[x]) != '':
          ProDetail[x] = req[x]
    if ProDetail:
      result['ProDetail'] = cPickle.dumps(ProDetail)

    Extra = {}
    for x in self.extraFields:
      if x in req and str(req[x]) != '':
        Extra[x] = req[x]
    if Extra:
      result['Extra'] = cPickle.dumps(Extra)

    return result

  def __request_types(self):
    types = ''
    csS = PathFinder.getServiceSection('ProductionManagement/ProductionRequest')
    if csS:
      types = gConfig.getValue('%s/RequestTypes' % csS, '')
    if not types:
      return ['Simulation', 'Reconstruction', 'Stripping']
    return types.split(', ')

  def __getArgument(self, argName, argDefValue):
    # ToDo: I think it should be somewhere in DIRAC
    if argName not in self.request.arguments:
      return argDefValue
    if self.request.arguments[argName]:
      return self.request.arguments[argName][0]
    return argDefValue

  def __getJsonArgument(self, argName, argDefValue):
    # ToDo: I think it should be somewhere in DIRAC
    if argName not in self.request.arguments:
      return argDefValue
    if self.request.arguments[argName]:
      return json.loads(self.request.arguments[argName][0])
    return argDefValue

  @staticmethod
  def __bkProductionProgress(prId):
    RPC = RPCClient('Bookkeeping/BookkeepingManager')
    result = RPC.getProductionInformations(prId)
    if not result['OK']:
      return result
    info = result['Value']
    if 'Number of events' not in info:
      return S_OK(0)
    allevents = info['Number of events']
    if not allevents:
      return S_OK(0)
    if len(allevents) > 1:
      return S_ERROR('More than one output file type. Unsupported.')
    return S_OK(allevents[0][1])

  def __2local(self, req):
    result = {}

    for x, y in zip(self.localFields, self.serviceFields):
      if isinstance(req[y], (datetime.date, datetime.datetime)):
        result[x] = str(req[y])
      else:
        result[x] = req[y]
    result['_is_leaf'] = not result['_is_leaf']
    if req['bkTotal'] is not None and req['rqTotal'] is not None and req['rqTotal']:
      result['progress'] = long(req['bkTotal']) * 100 / long(req['rqTotal'])
    else:
      result['progress'] = None
    if req['SimCondDetail']:
      try:
        result.update(cPickle.loads(req['SimCondDetail']))
      except cPickle.UnpicklingError:
        gLogger.error("Bad requests: simulation/data taking conditions is not defined", req['RequestID'])

    if req['ProDetail']:
      try:
        result.update(cPickle.loads(req['ProDetail']))
      except cPickle.UnpicklingError:
        gLogger.error("Bad requests: processing pass is not defined", req['RequestID'])

    if req['Extra']:
      result.update(cPickle.loads(req['Extra']))
    for x in result:
      if x != '_parent' and result[x] == None:
        result[x] = ''
    return result

  def __parseRequest(self):
    path = ''
    if 'fullpath' in self.request.arguments:
      path = self.request.arguments['fullpath'][-1]

    rType = False
    if 'type' in self.request.arguments:
      rType = True if self.request.arguments['type'][-1] == 'adv' else False

    tree = 'Configuration'
    if 'tree' in self.request.arguments:
      tree = self.request.arguments['tree'][-1]

    dataQuality = None
    if 'dataQuality' in self.request.arguments:
      dataQuality = dict(json.loads(self.request.arguments['dataQuality'][-1]))
      if not dataQuality:
        dataQuality = {'OK': True}

    if "limit" in self.request.arguments:
      self.numberOfJobs = int(self.request.arguments["limit"][-1])
      if "start" in self.request.arguments:
        self.pageNumber = int(self.request.arguments["start"][-1])
      else:
        self.pageNumber = 0
    else:
      self.numberOfJobs = 25
      self.pageNumber = 0
    return path, rType, tree, dataQuality

  def __condFromPath(self, path, bkcli, item):
    runCondTr = {'DaqperiodId': 'simCondID', 'Description': 'simDesc',
                 'BeamCondition': 'BeamCond', 'BeanEnergy': 'BeamEnergy',
                 'MagneticField': 'MagneticField',
                 'VELO': 'detVELO', 'IT': 'detIT', 'TT': 'detTT',
                 'OT': 'detOT', 'RICH1': 'detRICH1', 'RICH2': 'detRICH2',
                 'SPD_PRS': 'detSPD_PRS', 'ECAL': 'detECAL',
                 'HCAL': 'detHCAL', 'MUON': 'detMUON', 'L0': 'detL0',
                 'HLT': 'detHLT', 'VeloPosition': 'VeloPosition'}
    simCondTr = {'BeamEnergy': 'BeamEnergy', 'Description': 'simDesc',
                 'Generator': 'Generator', 'Luminosity': 'Luminosity',
                 'G4settings': 'G4settings',
                 'MagneticField': 'MagneticField',
                 'DetectorCondition': 'DetectorCond',
                 'BeamCondition': 'BeamCond',
                 'SimId': 'simCondID'}

    p = path.split('/')
    condp = '/'.join(p[0:4])
    if not item:
      dirp = '/'.join(p[0:3])
      allcond = bkcli.list(dirp)
      cond = {}
      for x in allcond:
        if x['fullpath'] == condp:
          cond = x
          break
      if not cond:
        return S_ERROR("Could not find %s" % condp)
    else:
      cond = item
    if cond['level'] != 'Simulation Conditions/DataTaking':
      return S_ERROR("%s is not a condition" % condp)
    for x in ['level', 'expandable', 'name', 'fullpath', 'selection', 'method']:
      if x in cond:
        del cond[x]
    if 'DaqperiodId' in cond:
      tr, rType = runCondTr, 'Run'
    else:
      tr, rType = simCondTr, 'Simulation'
    nm = [x for x in cond if x not in tr]
    if nm:
      return S_ERROR("Unmached run conditions: %s" % str(nm))
    v = dict([(tr[x], cond[x]) for x in cond])
    v['condType'] = rType
    if rType == 'Run':
      v['DetectorCond'] = self.__runDetectorCond(v)
    return S_OK(v)

  def __runDetectorCond(self, cond):
    subd = ['detVELO', 'detIT', 'detTT', 'detOT',
            'detRICH1', 'detRICH2', 'detSPD_PRS',
            'detECAL', 'detHCAL', 'detMUON',
            'detL0', 'detHLT']
    incl = []
    not_incl = []
    unknown = []
    for x in subd:
      if x not in cond:
        unknown.append(x[3:])
      elif cond[x] == 'INCLUDED':
        incl.append(x[3:])
      elif cond[x] == 'NOT INCLUDED':
        not_incl.append(x[3:])
      else:
        unknown.append(x[3:])
    if len(incl) > len(not_incl):
      if not not_incl:
        s = "all"
        if unknown:
          s += " except unknown %s" % ','.join(unknown)
      else:
        s = "without %s" % ','.join(not_incl)
        if unknown:
          s += " and unknown %s" % ','.join(unknown)
    else:
      if incl:
        s = "only %s" % ','.join(incl)
        if unknown:
          s += " and unknown %s" % ','.join(unknown)
      else:
        s = "no"
        if unknown:
          s += " with unknown %s" % ','.join(unknown)
    return s

  def __prepareTTList(self, values):
    """ Prepare a list of templates or tests for sending to the interface """
    dvalue = {}  # Remove "_run.py" templates from the list and set type
    for x in values:
      x['Type'] = 'Simple'
      dvalue[x['WFName']] = x
    value = []
    for x in dvalue:
      if '_run.py' in x:
        master = x.replace('_run.py', '')
        if master in dvalue:
          dvalue[master]['Type'] = 'Combi'
          continue
        dvalue[x]['Type'] = 'Script'
      value.append(dvalue[x])
    return {'OK': True, 'total': len(value), 'result': value}

  @asyncGen
  def web_getSelectionData(self):
    callback = {}
    RPC = RPCClient("ProductionManagement/ProductionRequest")
    retVal = yield self.threadTask(RPC.getFilterOptions)
    if not retVal['OK']:
      self.finish({"success": "false", "result": [], "total": 0, "error": retVal["Message"]})
      return

    callback['typeF'] = [[i] for i in retVal['Value']['Type']]
    callback['stateF'] = [[i] for i in retVal['Value']['State']]
    callback['authorF'] = [[i] for i in retVal['Value']['Author']]
    callback['wgF'] = [[i] for i in retVal['Value']['WG']]

    self.write(callback)

  @asyncGen
  def web_getWG(self):
    RPC = RPCClient("ProductionManagement/ProductionRequest")
    retVal = yield self.threadTask(RPC.getFilterOptions)
    if not retVal['OK']:
      raise WErr.fromSERROR(retVal)
    rows = [{"text": i, "name": i} for i in retVal['Value']['WG']]
    self.finish({'OK': True, 'result': rows, 'total': len(rows)})

  @asyncGen
  def web_list(self):
    parent = self.request.arguments.get('anode', 0)
    try:
      if parent != 0:
        parent = json.loads(parent[-1])
        if isinstance(parent, list):
          parent = parent[-1]
    except Exception, e:
      raise WErr(404, e)

    result = yield self.threadTask(self.getProductionRequestList, parent)
    self.finish(result)

  @asyncGen
  def web_history(self):
    requestID = self.request.arguments.get('RequestID', [''])
    try:
      requestID = long(json.loads(requestID[-1]))
    except Exception as _e:
      self.finish({"success": "false", "result": [], "total": 0, "error": 'Request ID is not a number'})

    RPC = RPCClient("ProductionManagement/ProductionRequest")
    result = yield self.threadTask(RPC.getRequestHistory, requestID)
    if not result['OK']:
      self.finish({"success": "false", "result": [], "total": 0, "error": result["Message"]})
      return
    rows = result['Value']['Rows']
    result = []
    for row in rows:
      row['TimeStamp'] = str(row['TimeStamp'])
      result += [[row["TimeStamp"], row["RequestState"], row["RequestUser"]]]
    self.finish({"success": "true", "result": result, "total": len(result)})

  @asyncGen
  def web_duplicate(self):
    reqId = self.request.arguments.get('ID', '')
    clearpp = self.request.arguments.get('ClearPP', False)
    try:
      reqId = long(json.loads(reqId[-1]))
      clearpp = json.loads(clearpp[-1])
    except Exception as _e:
      self.finish({"success": "false", "result": [], "total": 0, "error": 'Request ID is not a number'})

    RPC = RPCClient("ProductionManagement/ProductionRequest")
    result = yield self.threadTask(RPC.duplicateProductionRequest, reqId, clearpp)
    self.write(result)

  @asyncGen
  def web_delete(self):
    reqId = self.request.arguments.get('ID', '')
    try:
      reqId = long(json.loads(reqId[-1]))
    except Exception as _e:
      self.finish({"success": "false", "result": [], "total": 0, "error": 'Request ID is not a number'})
    RPC = RPCClient("ProductionManagement/ProductionRequest")
    result = yield self.threadTask(RPC.deleteProductionRequest, reqId)
    self.write(result)

  @asyncGen
  def web_typeandmodels(self):
    types = [(x, x) for x in self.__request_types()]
    RPC = RPCClient("ProductionManagement/ProductionRequest")
    result = yield self.threadTask(RPC.getProductionRequestList,
                                   long(0),
                                   'RequestID', 'DESC',
                                   long(0),
                                   long(0),
                                   {'IsModel': 1})
    models = []
    if not result['OK']:
      models = []
    else:
      models = [(str(x['RequestID']), str(x['RequestType'] + ' - ' + x['RequestName']))
                for x in result['Value']['Rows']]

    rows = [{'Name': x[0], 'Description':x[1]} for x in types + models]
    self.finish({'OK': True, 'result': rows, 'total': len(rows)})

  @asyncGen
  def web_bkk_dq_list(self):
    RPC = RPCClient('Bookkeeping/BookkeepingManager')
    result = yield self.threadTask(RPC.getAvailableDataQuality)
    if not result['OK']:
      self.finish({"success": "false", "result": [], "total": 0, "error": result['Message']})
      return
    value = []
    if result['Value']:
      value.append({'v': 'ALL'})
    for x in result['Value']:
      value.append({'v': x})
    self.finish({'OK': True, 'total': len(value), 'result': value})

  @asyncGen
  def web_bkk_event_types(self):
    addempty = 'addempty' in self.request.arguments
    RPC = RPCClient('Bookkeeping/BookkeepingManager')
    result = yield self.threadTask(RPC.getAvailableEventTypes)
    if not result['OK']:
      self.finish({"success": "false", "result": [], "total": 0, "error": result['Message']})
      return
    rows = []
    for et in result['Value']:
      rows.append({'id': et[0],
                   'name': et[1],
                   'text': '%s - %s' % (str(et[0]), str(et[1]))
                   })
    rows.sort(key=lambda x: x['id'])
    if addempty:
      rows.insert(0, {'id': 99999999, 'name': '', 'text': '&nbsp;'})
    self.finish({'OK': True, 'result': rows, 'total': len(rows)})

  @asyncGen
  def web_bkk_input_tcks(self):
    RPC = RPCClient('Bookkeeping/BookkeepingManager')

    pars = {
        'EventTypeId': str(self.request.arguments.get('eventType', [''])[-1]),
        'ConfigVersion': str(self.request.arguments.get('configVersion', [''])[-1]),
        'ProcessingPass': '/' + str(self.request.arguments.get('inProPass', [''])[-1]),
        'ConfigName': str(self.request.arguments.get('configName', [''])[-1]),
        'ConditionDescription': str(self.request.arguments.get('simDesc', [''])[-1])
    }
    # log.info(str(pars))
    result = yield self.threadTask(RPC.getTCKs, pars)
    if not result['OK']:
      self.finish({"success": "false", "result": [], "total": 0, "error": result['Message']})
      return
    gLogger.info(result['Value'])
    value = []
    if result['Value']:
      value.append({'id': 0, 'text': 'ALL'})
    for x in result['Value']:
      value.append({'id': str(x), 'text': str(x)})
    value.sort(key=lambda x: x['id'])
    self.finish({'OK': True, 'total': len(value), 'result': value})

  @asyncGen
  def web_bkk_input_prod(self):
    RPC = RPCClient('Bookkeeping/BookkeepingManager')

    pars = {
        'EventTypeId': str(self.request.arguments.get('eventType', [''])[-1]),
        'ConfigVersion': str(self.request.arguments.get('configVersion', [''])[-1]),
        'ProcessingPass': '/' + str(self.request.arguments.get('inProPass', [''])[-1]),
        'ConfigName': str(self.request.arguments.get('configName', [''])[-1]),
        'ConditionDescription': str(self.request.arguments.get('simDesc', [''])[-1])
    }
    gLogger.info(str(pars))
    result = yield self.threadTask(RPC.getProductions, pars)
    if not result['OK']:
      self.finish({"success": "false", "result": [], "total": 0, "error": result['Message']})
      return
    value = []
    if result['Value']['Records']:
      value.append({'id': 0, 'text': 'ALL'})
    for x in result['Value']['Records']:
      prod = x[0]
      if prod < 0:
        prod = -prod
      value.append({'id': prod, 'text': prod})
    value.sort(key=lambda x: x['id'])
    self.finish({'OK': True, 'total': len(value), 'result': value})

  @asyncGen
  def web_bkk_simcond(self):
    RPC = RPCClient('Bookkeeping/BookkeepingManager')
    result = yield self.threadTask(RPC.getSimConditions)
    if not result['OK']:
      self.finish({"success": "false", "result": [], "total": 0, "error": result['Message']})
      return
    rows = []
    for sc in result['Value']:
      rows.append(dict(zip(self.bkSimCondFields, sc)))
#    !!! Sorting and selection must be moved to MySQL/Service side
    result = SelectAndSort(self.request, rows, 'simCondID')
    self.finish(result)

  @asyncGen
  def web_getSimCondTree(self):

    _path, rtype, tree, dataQuality = self.__parseRequest()

    node = ''
    if 'node' in self.request.arguments:
      node = self.request.arguments['node'][-1]

    bk = LHCB_BKKDBClient(web=True)

    yield self.threadTask(bk.setFileTypes, [])

    bk.setAdvancedQueries(rtype)
    bk.setParameter(tree)
    bk.setDataQualities(dataQuality)

    retVal = yield self.threadTask(bk.list, node)

    nodes = []
    if retVal:
      for i in retVal:
        node = {}
        node['text'] = i['name']
        node['fullpath'] = i['fullpath']
        node['id'] = i['fullpath']
        node['selection'] = i['selection'] if 'selection' in i else ''
        node['method'] = i['method'] if 'method' in i else ''
        node['cls'] = "folder" if i['expandable'] else 'file'
        if 'level' in i and i['level'] == 'Simulation Conditions/DataTaking':
          node['cls'] = 'file'
          node['leaf'] = True
        else:
          node['leaf'] = False if i['expandable'] else True
        if 'level' in i:
          node['level'] = i['level']
          node['qtip'] = i['level']
        nodes += [node]

    result = tornado.escape.json_encode(nodes)
    self.finish(result)

  @asyncGen
  def web_bkk_input_tree(self):
    keep = ['simCondID', 'condType', 'inProPass', 'evType']

    node = ''
    if 'node' in self.request.arguments:
      node = self.request.arguments['node'][-1]

    bkcli = LHCB_BKKDBClient(web=True)

    value = []
    level = ''
    scd = {}
    known = {}

    nodes = yield self.threadTask(bkcli.list, node)
    for item in nodes:
      ipath = item['fullpath']
      p = ipath.split('/')[1:]
      x = {'id': ipath, 'text': p[-1]}
      x.update(dict(zip(['configName', 'configVersion'], p[:2])))
      for i in keep:
        if self.request.arguments.get(i, ''):
          x[i] = self.request.arguments.get(i)
      if 'level' in item:
        level = item['level']
      if level == 'Simulation Conditions/DataTaking':
        result = self.__condFromPath(ipath, bkcli, item)
        if not result['OK']:
          raise WErr.fromSERROR(result)
        x.update(result['Value'])
      elif level == 'Processing Pass':
        if x.get('inProPass', ''):
          x['inProPass'] = x['inProPass'][-1] + '/' + p[-1]
        else:
          x['inProPass'] = p[-1]
      elif level == 'Event types':
        x['evType'] = p[-1]
      elif level == 'FileTypes':
        x['leaf'] = True
        x['inFileType'] = p[-1]
        if not scd:
          result = self.__condFromPath(ipath, bkcli, {})
          if not result['OK']:
            raise WErr.fromSERROR(result)
          scd = result['Value']
        x.update(scd)
      if x['text'] in known:
        gLogger.error("Duplicated entry: %s %s" % (str(known[x['text']]), str(x)))
      else:
        known[x['text']] = x
        value.append(x)
    result = tornado.escape.json_encode(value)
    self.finish(result)

  @asyncGen
  def web_save(self):

    rdict = dict(self.request.arguments)
    self.__unescape(rdict)

    # Make it backward compatible
    for x in ['inTCKs']:
      if x in rdict and str(rdict[x]) == '0':
        del rdict[x]
    reqId = ''
    if 'ID' in rdict:
      try:
        reqId = long(rdict['ID'])
      except Exception:
        self.finish({"success": "false", "result": [], "total": 0, "error": "Request ID is not a number"})
        return
      del rdict['ID']
      RPC = RPCClient("ProductionManagement/ProductionRequest")
      req = self.__fromlocal(rdict)
      result = yield self.threadTask(RPC.updateProductionRequest, reqId, req)
    else:
      RPC = RPCClient("ProductionManagement/ProductionRequest")

      sData = self.getSessionData()

      user = sData["user"]["username"]

      rdict['RequestAuthor'] = user
      req = self.__fromlocal(rdict)
      result = yield self.threadTask(RPC.createProductionRequest, req)

    if result['OK']:
      self.finish({'success': "true", "requestId": reqId})
    else:
      raise WErr.fromSERROR(result)
    # self.finish()

  @asyncGen
  def web_templates(self):
    RPC = RPCClient("ProductionManagement/ProductionRequest")
    ret = yield self.threadTask(RPC.getProductionTemplateList)
    if not ret['OK']:
      raise WErr.fromSERROR(ret)
    result = self.__prepareTTList(ret['Value'])
    self.finish(result)

  def getRequestFields(self):
    result = dict.fromkeys(self.localFields, None)
    result.update(dict.fromkeys(self.simcondFields))
    result.update(dict.fromkeys(self.extraFields))
    for x in self.proStepFields:
      for i in range(1, 20):
        result['p%d%s' % (i, x)] = None
    sData = self.getSessionData()

    result['userName'] = sData["user"]["username"]
    result['userDN'] = sData["user"]["DN"]
    return result

  @asyncGen
  def web_template_parlist(self):
    tpl_name = str(self.request.arguments.get('tpl', [''])[-1])
    RPC = RPCClient("ProductionManagement/ProductionRequest")
    result = yield self.threadTask(RPC.getProductionTemplate, tpl_name)
    if not result['OK']:
      raise WErr.fromSERROR(result)
    text = result['Value']
    result = yield self.threadTask(RPC.getProductionTemplate, tpl_name + '_run.py')
    if result['OK']:
      text += result['Value']
    tpl = PrTpl(text)
    rqf = self.getRequestFields()
    tpf = tpl.getParams()
    tpd = tpl.getDefaults()
    plist = []
    for x in tpf:
      if x not in rqf:
        plist.append({'par': x, 'label': tpf[x], 'value': tpd[x].split('^')[0], 'default': tpd[x]})
    self.finish({'OK': True, 'total': len(plist), 'result': plist})

  def getProductionRequest(self, ids):
    RPC = RPCClient("ProductionManagement/ProductionRequest")
    result = RPC.getProductionRequest(ids)
    if not result['OK']:
      return result
    rr = result['Value']
    lr = {}
    for x in rr:
      lr[x] = self.__2local(rr[x])
    return S_OK(lr)

  def getProductionRequestList(self, parent):
    try:
      offset = int(self.__getArgument('start', 0))
      limit = int(self.__getArgument('limit', 0))
      sortlist = self.__getJsonArgument('sort', [])
      if sortlist:
        sortBy = str(sortlist[-1]['property'])
        sortBy = self.serviceFields[self.localFields.index(sortBy)]
        sortOrder = str(sortlist[-1]['direction'])
      else:
        sortBy = self.serviceFields[self.localFields.index("ID")]
        sortOrder = "ASC"

      filterOpts = {}
      for x, y in [('typeF', 'RequestType'), ('stateF', 'RequestState'),
                   ('authorF', 'RequestAuthor'), ('idF', 'RequestID'), ('modF', 'IsModel'),
                   ('wgF', 'RequestWG')]:

        val = self.request.arguments.get(x, "")

        if val != '':
          val = list(json.loads(val[-1]))
          if val:
            if not isinstance(val[0], bool):
              filterOpts[y] = ','.join(val)
            else:
              filterOpts[y] = val[-1]

      if 'IsModel' in filterOpts:
        if filterOpts['IsModel']:
          filterOpts['IsModel'] = 1
        else:
          filterOpts['IsModel'] = 0
    except Exception, e:
      raise WErr(404, 'Badly formatted list request: %s' + str(e))

    RPC = RPCClient("ProductionManagement/ProductionRequest")

    result = RPC.getProductionRequestList(parent, sortBy, sortOrder, offset, limit, filterOpts)

    if not result['OK']:
      raise WErr.fromSERROR(result)

    rows = [self.__2local(x) for x in result['Value']['Rows']]
    RPC = RPCClient('Bookkeeping/BookkeepingManager')
    aet = RPC.getAvailableEventTypes()
    if aet['OK']:
      etd = {}
      for et in aet['Value']:
        etd[str(et[0])] = str(et[1])
      for x in rows:
        if 'eventType' in x:
          x['eventText'] = etd.get(str(x['eventType']), '')

    return {'OK': True, 'result': rows, 'total': result['Value']['Total']}

  @asyncGen
  def web_create_workflow(self):
    """ !!! Note: 1 level parent=master assumed !!! """
    params = dict(self.request.arguments)
    rdict = {}
    for i in params:
      rdict[i] = params[i][-1]

    for x in ['RequestID', 'Template', 'Subrequests']:
      if x not in rdict:
        raise WErr(404, 'Required parameter %s is not specified' % x)
    try:
      reqId = long(rdict['RequestID'])
      tpl_name = str(self.request.arguments['Template'][-1])
      operation = str(self.request.arguments.get('Operation', ['Simple'])[-1])
      sstr = str(self.request.arguments['Subrequests'][-1])
      if sstr:
        slist = [long(x) for x in sstr.split(',')]
      else:
        slist = []
      sdict = dict.fromkeys(slist, None)
      del rdict['RequestID']
      del rdict['Template']
      del rdict['Subrequests']
      if 'Operation' in rdict:
        del rdict['Operation']
    except Exception as e:
      raise WErr(404, 'Wrong parameters (%s)' % str(e))
    requests = []
    if 'RequestIDs' in rdict:
      requests = json.loads(rdict['RequestIDs'])
    if requests:
      res = []
      for reqId in requests:
        reqId = long(reqId)
        retVal = yield self.threadTask(self.__createScript, rdict, reqId, operation, tpl_name, sdict)
        if retVal['OK']:
          res += [val for val in retVal['Value']]
        else:
          raise WErr(404, 'The following error occurred during the production creation: %s' % retVal['Message'])
        if operation == 'ScriptPreview':
          break
      self.finish(S_OK(res))
    else:
      retVal = yield self.threadTask(self.__createScript, rdict, reqId, operation, tpl_name, sdict)
      if not retVal['OK']:
        raise WErr(404, 'The following error occurred during the production creation: %s' % retVal['Message'])
      self.finish(retVal)

  def __createScript(self, rdict, reqId, operation, tpl_name, sdict):
    """
    This method is used to create production or view the script using a certain template
    :param dict rdict: contains the production parameters
    :param long reqId: request id
    :param str operation: script previre or generate the production
    :param str tpl_name: template name use to create the workflow
    :param dict sdict: sub request parameters
    :return: S_OK(requestID:long, Body:list}
    """
    RPC = RPCClient("ProductionManagement/ProductionRequest")
    ret = RPC.getProductionTemplate(tpl_name)
    if not ret['OK']:
      raise WErr.fromSERROR(ret)

    tpl = PrTpl(ret['Value'])
    run_tpl = None
    ret = RPC.getProductionTemplate(tpl_name + '_run.py')
    if ret['OK']:
      run_tpl = PrTpl(ret['Value'])

    ret = self.getProductionRequest([reqId])
    if not ret['OK']:
      raise WErr.fromSERROR(ret)
    rqdict = ret['Value'][reqId]
    dictlist = []
    if rqdict['_is_leaf']:
      d = self.getRequestFields()
      d.update(rqdict)
      d.update(rdict)
      dictlist.append(d)
    else:
      if not sdict:
        raise WErr(404, 'Subrequests are not specified (but required)')
      ret = self.getProductionRequestList(reqId)
      if not ret['OK']:
        raise WErr.fromSERROR(ret)
      for x in ret['result']:
        if x['ID'] not in sdict:
          continue
        d = self.getRequestFields()
        d.update(rqdict)
        for y in x:
          if x[y]:
            d[y] = x[y]
        d.update(rdict)
        dictlist.append(d)
    success = []
    fail = []
    RPC = RPCClient("ProductionManagement/ProductionRequest")
    for x in dictlist:
      for y in x:
        if x[y] == None:
          x[y] = ''
        else:
          x[y] = str(x[y])
      if not run_tpl and '_run.py' not in tpl_name:
        success.append({'ID': x['ID'], 'Body': tpl.apply(x)})
      elif operation != 'Generate':
        if operation == 'ScriptPreview' and run_tpl:
          success.append({'ID': x['ID'], 'Body': run_tpl.apply(x)})
        else:
          success.append({'ID': x['ID'], 'Body': tpl.apply(x)})
      else:
        try:
          if not run_tpl:
            res = RPC.execProductionScript(tpl.apply(x), "")
          else:
            res = RPC.execProductionScript(run_tpl.apply(x), tpl.apply(x))
          if res['OK']:
            success.append({'ID': x['ID'], 'Body': res['Value']})
          else:
            fail.append(str(x['ID']))
        except Exception as _msg:
          fail.append(str(x['ID']))
      continue  # not working with WF DB for now
      # name = 'PRQ_%s' % x['ID']
      # wf = fromXMLString( tpl.apply( x ) )
      # wf.setName( name )
      # wf.setType( 'production/requests' )
      # try:
      #   wfxml = wf.toXML()
      #   result = yield self.threadTask( RPC.publishWorkflow, str( wfxml ), True )
      # except Exception, msg:
      #   result = {'OK':False, 'Message': str( msg )}
      # if not result['OK']:
      #   fail.append( str( x['ID'] ) )
      # else:
      #   success.append( str( x['ID'] ) )

    if fail:
      raise WErr(404, "Couldn't get results from %s."
                 % (','.join(fail)))
    else:
      # return S_OK("Success with %s" % ','.join(success))
      return S_OK(success)

  @asyncGen
  def web_split(self):
    try:
      reqId = long(self.request.arguments['ID'][-1])
      slist = list(json.loads(self.request.arguments['Subrequests'][-1]))
    except Exception as e:
      raise WErr(500, 'Wrong parameters (%s)' % str(e))

    RPC = RPCClient("ProductionManagement/ProductionRequest")
    result = yield self.threadTask(RPC.splitProductionRequest, reqId, slist)
    self.finish(result)

  @asyncGen
  def web_progress(self):

    try:
      reqId = long(self.request.arguments.get('RequestID', [0])[-1])
    except Exception:
      raise WErr(500, 'Request ID is not a number')
    RPC = RPCClient("ProductionManagement/ProductionRequest")
    result = yield self.threadTask(RPC.getProductionProgressList, reqId)
    if not result['OK']:
      raise WErr.fromSERROR(result)
    self.finish({'OK': True, 'result': result['Value']['Rows'],
                 'total': result['Value']['Total']})

  @asyncGen
  def web_add_production(self):

    try:
      requestID = long(self.request.arguments['RequestID'][-1])
      productionID = long(self.request.arguments['ProductionID'][-1])
    except:
      raise WErr(500, 'Incorrect Request or production ID')

    result = yield self.threadTask(self.__bkProductionProgress, productionID)
    if not result['OK']:
      bkEvents = 0
    else:
      bkEvents = result['Value']

    RPC = RPCClient("ProductionManagement/ProductionRequest")
    result = yield self.threadTask(RPC.addProductionToRequest, {
        'ProductionID': productionID, 'RequestID': requestID,
        'Used': 1, 'BkEvents': bkEvents
    })
    self.finish(result)

  @asyncGen
  def web_remove_production(self):

    try:
      productionID = long(self.request.arguments['ProductionID'][-1])
    except:
      raise WErr(500, 'Production ID is not a number')

    RPC = RPCClient("ProductionManagement/ProductionRequest")
    result = yield self.threadTask(RPC.removeProductionFromRequest, productionID)
    self.finish(result)

  @asyncGen
  def web_use_production(self):
    try:
      productionID = long(self.request.arguments['ProductionID'][-1])
      used = bool(self.request.arguments.get('Used', [True])[-1])
    except:
      raise WErr(500, 'Incorrect Production ID use Used flag')

    RPC = RPCClient("ProductionManagement/ProductionRequest")
    result = yield self.threadTask(RPC.useProductionForRequest, productionID, used)
    self.finish(result)

  @asyncGen
  def web_productions(self):
    try:
      prId = long(self.request.arguments['RequestID'][-1])
    except Exception as _e:
      raise WErr(500, 'Request ID is not a number')

    RPC = RPCClient("ProductionManagement/ProductionRequest")
    result = yield self.threadTask(RPC.getProductionList, prId)
    if not result['OK']:
      raise WErr.fromSERROR(result)
    self.finish({'OK': True, 'result': result['Value'], 'total': len(result['Value'])})

  @asyncGen
  def web_getRequest(self):
    try:
      stepId = self.request.arguments['StepId'][-1]
    except:
      raise WErr(500, 'Incorrect Production ID use Used flag')

    requests = ProductionRequestManagerHandler.__dataCache.get("allData")
    if not requests:
      RPC = RPCClient("ProductionManagement/ProductionRequest")
      retVal = yield self.threadTask(RPC.getProductionRequestList, 0, '', '', 0, 0, {"RequestType": "Simulation"})
      if not retVal['OK']:
        raise WErr.fromSERROR(retVal)
      requests = retVal['Value'].get('Rows', [])
      ProductionRequestManagerHandler.__dataCache.add("allData", 3600, requests)

    records = []
    for request in requests:
      if request.get("ProDetail") and str(stepId) in request.get("ProDetail", ""):
        record = {}
        record["RequestId"] = request["RequestID"]
        record["RequestName"] = request["RequestName"]
        record["RequestWG"] = request["RequestWG"]
        record["RequestAuthor"] = request["RequestAuthor"]
        records.append(record)
    self.finish({"success": "true", "result": records, "total": len(
        records), "date": Time.dateTime().strftime("%Y-%m-%d %H:%M [UTC]")})

  @asyncGen
  def web_getFastSimulationOpts(self):
    """

    This method used by the "Fast simulation type" combo box. The values of the combo box is filled using the CS

    """

    fastsimTypes = ["None", "Redecay"]
    csS = PathFinder.getServiceSection('ProductionManagement/ProductionRequest')
    if csS:
      fastsimTypes = gConfig.getValue('%s/FastSimulationType' % csS, fastsimTypes)

    rows = [{"text": i, "name": i} for i in fastsimTypes]
    self.finish({'OK': True, 'result': rows, 'total': len(rows)})
