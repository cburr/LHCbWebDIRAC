Ext.define('LHCbDIRAC.Accounting.classes.ImageModel', {
  extend: 'Ext.data.Model',
  fields: [
           { name:'src', type:'string' },
           { name:'name', type:'string' },
           { name: 'size', type: 'float'},
           { name: 'lastmod', type:'date', dateFormat:'timestamp'},
           { name: 'plotParams', type:'object'}
           ]
});