/**
 * This class is used to show images on a View. It allows to select multiple images and as well drag and drop.
 */
Ext.define('LHCbDIRAC.Accounting.classes.Presenter', {
  extend : 'Ext.panel.Panel',
  requires : ['LHCbDIRAC.Accounting.classes.ImageModel', 'Ext.XTemplate','Ext.panel.Panel', 'LHCbDIRAC.Accounting.classes.ImageDragDrop'],
  autoScroll : true,
  //renderTo : Ext.getBody(),
  index : 0,
  layout : 'fit',
  initComponent : function(){
    var me = this;

    var imagesStore = Ext.create('Ext.data.Store',{
      model: 'LHCbDIRAC.Accounting.classes.ImageModel'});

    var imageTpl = Ext.create('Ext.XTemplate',
        '<tpl for=".">',
        '<div class="plot">',
        (!Ext.isIE6? '<img width="100%" height="100%" src="{src}" />' :
        '<div style="width:100px;height:100px;filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src="{src}",sizingMethod=\'scale\')"></div>'),
        '<span class="x-editable"><strong>{shortName:htmlEncode}</strong></span>',
        '</div>',
        '</tpl>'
    );

   me.view = Ext.create('Ext.view.View', {
      store: imagesStore,
      tpl:imageTpl,
      //autoRender : true,
      multiSelect: true,
      emptyText: 'No images to display',
      id: 'plots',
      itemSelector: 'div.plot',
      overItemCls : 'plot-hover',
      autoScroll  : true,
      plugins: {
        ptype: 'imagedragdrop'
      },
      listeners : {
        itemclick : function( record, item, index, e, eOpts ){
          alert('COOOOLLLLL na ez van!!!');
        }
      }

    });
    Ext.apply(me, {
      items: [me.view],
      frame: true,
      collapsible: true
    });

    me.callParent(arguments);
  },
  addImage:function(img){
    var me = this;
    var store = me.view.getStore();
    store.add(img);
    me.view.refresh();
  }

});