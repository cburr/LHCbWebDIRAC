Ext.define('LHCbDIRAC.ProductionRequestManager.classes.RequestModelWindow', {
      extend : 'Ext.window.Window',

      alias : 'widget.requestwindow',

      plain : true,
      resizable : false,
      modal : true,
      closeAction : 'hide',

      initComponent : function() {
        var me = this;
        me.addEvents('create');
        me.defaultModels = Ext.create("Ext.data.JsonStore", {
              fields : [{
                    name : 'Name'
                  }, {
                    name : 'Description'
                  }],
              proxy : {
                type : 'ajax',
                url : GLOBAL.BASE_URL + 'ProductionRequestManager/typeandmodels',
                reader : {
                  type : 'json',
                  root : 'result'
                }
              }
            });
        me.form = Ext.create('widget.form', {
              bodyPadding : '12 10 10',
              border : false,
              unstyled : true,
              items : [{
                    anchor : '100%',
                    fieldLabel : 'Request Type',
                    name : 'reqType',
                    forceSelection : true,
                    displayField : 'Description',
                    valueField : 'Name',
                    labelAlign : 'top',
                    msgTarget : 'under',
                    xtype : 'combo',
                    store : me.defaultModels
                  }]
            });
        Ext.apply(me, {
              width : 500,
              title : 'Please select Request Type or model',
              layout : 'fit',
              items : me.form,
              buttons : [{
                    xtype : 'button',
                    text : 'Create request',
                    scope : me,
                    handler : me.onCreateClick
                  }, {
                    xtype : 'button',
                    text : 'Cancel',
                    scope : me,
                    handler : me.doHide
                  }]
            });
        me.callParent(arguments);
      },

      doHide : function() {
        this.hide();
      },

      onCreateClick : function(addBtn) {
        var me = this;
        var value = me.form.getValues()["reqType"];
        if (value == '')
          return;
        me.fireEvent('create', value);
        me.close();
        me.destroy();
      }

    });