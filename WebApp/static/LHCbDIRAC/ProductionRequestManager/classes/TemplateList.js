Ext.define('LHCbDIRAC.ProductionRequestManager.classes.TemplateList', {
      extend : "Ext.grid.Panel",

      initComponent : function() {
        var me = this;

        me.store = Ext.create("Ext.data.JsonStore", {
              remoteSort : false,
              fields : ["AuthorGroup", "Author", "PublishingTime", "LongDescription", "WFName", "AuthorDN", "WFParent", "Description", "Type"],
              proxy : {
                type : 'ajax',
                url : GLOBAL.BASE_URL + 'ProductionRequestManager/templates',
                reader : {
                  type : 'json',
                  root : 'result'
                }
              }
            });

        me.store.load();

        Ext.apply(this, {
              columns : [{
                    header : 'Template',
                    sortable : true,
                    dataIndex : 'WFName',
                    flex : true,
                    renderer : function(value) {
                      return value.replace(/_wizard\.py/, '');
                    }
                  }],
              autoHeight : false,
              autoWidth : true,
              loadMask : true,
              region : 'center',
              store : me.store,
              stripeRows : true,
              viewConfig : {
                forceFit : true
              }
            });
        me.callParent(arguments);
      }
    });