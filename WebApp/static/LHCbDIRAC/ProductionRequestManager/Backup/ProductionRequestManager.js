Ext.define('LHCbDIRAC.ProductionRequestManager.classes.ProductionRequestManager', {
      extend : 'Ext.dirac.core.Module',

      requires : ["Ext.panel.Panel", "Ext.form.field.Date", "Ext.grid.plugin.RowExpander", "Ext.dirac.utils.DiracBaseSelector", "Ext.dirac.utils.DiracAjaxProxy", "Ext.dirac.utils.DiracJsonStore", "Ext.dirac.utils.DiracGridPanel", "Ext.dirac.utils.DiracPagingToolbar",
          "Ext.dirac.utils.DiracApplicationContextMenu", "Ext.dirac.utils.DiracRowExpander", "LHCbDIRAC.ProductionRequestManager.classes.LHCbRowExpander", "LHCbDIRAC.ProductionRequestManager.classes.LHCbTreeStore"],

      loadState : function(data) {
        var me = this;
        me.gridPanel.loadState(data);
        me.leftPanel.loadState(data);
        if (data.leftPanelCollapsed) {
          me.left.panel.collapse();
        }

      },

      getStateData : function() {
        var me = this;
        var oReturn = {
          leftMenu : me.leftPanel.getStateData(),
          grid : me.gridPanel.getStateData(),
          leftPanelCollapsed : me.leftPanel.collapsed
        };
        return oReturn;
      },

      dataFields : [],
      initComponent : function() {
        var me = this;
        var oDimensions = GLOBAL.APP.MAIN_VIEW.getViewMainDimensions();

        me.launcher.title = "Production Request manager";
        me.launcher.maximized = false;
        me.launcher.width = oDimensions[0];
        if (GLOBAL.VIEW_ID == "desktop") {
          me.launcher.height = oDimensions[1];
        } else {
          me.launcher.height = oDimensions[1] - GLOBAL.APP.MAIN_VIEW.taskbar.getHeight();
        }
        me.launcher.x = 0;
        me.launcher.y = 0;

        Ext.apply(me, {
              layout : 'card',
              bodyBorder : false
            });
        me.callParent(arguments);
      },

      dataFields : [{
            name : 'IdCheckBox',
            mapping : 'ID'
          }, {
            name : 'ID',
            type : 'auto'
          }, {
            name : 'reqName'
          }, {
            name : 'reqType'
          }, {
            name : 'reqState'
          }, {
            name : 'reqPrio'
          }, {
            name : 'reqAuthor'
          }, {
            name : 'reqWG'
          },

          {
            name : 'simCondID'
          }, {
            name : 'simDesc'
          }, {
            name : 'Generator'
          }, {
            name : 'G4settings'
          }, {
            name : 'MagneticField'
          }, {
            name : 'BeamEnergy'
          }, {
            name : 'Luminosity'
          }, {
            name : 'DetectorCond'
          }, {
            name : 'BeamCond'
          },

          {
            name : 'configName'
          }, {
            name : 'configVersion'
          }, {
            name : 'condType'
          }, {
            name : 'inProPass'
          }, {
            name : 'inFileType'
          }, {
            name : 'inProductionID'
          }, {
            name : 'inDataQualityFlag'
          }, {
            name : 'inTCKs'
          },

          {
            name : 'pID'
          }, {
            name : 'pDsc'
          }, {
            name : 'pAll'
          },

          {
            name : 'eventType'
          }, {
            name : 'eventText'
          }, {
            name : 'eventNumber'
          },

          {
            name : 'reqComment'
          }, {
            name : 'reqDesc'
          }, {
            name : 'reqInform'
          },

          {
            name : 'IsModel'
          }, {
            name : 'mcConfigVersion'
          },

          {
            name : 'eventBK'
          }, {
            name : 'EventNumberTotal'
          }, {
            name : 'eventBKTotal'
          }, {
            name : 'progress'
          }, {
            name : 'creationTime',
            type : 'date',
            dateFormat : 'Y-n-j h:i:s'
          }, {
            name : 'lastUpdateTime',
            type : 'date',
            dateFormat : 'Y-n-j h:i:s'
          },

          {
            name : 'TestState'
          }, {
            name : 'TestActual'
          },

          {
            name : '_parent',
            type : 'auto'
          }, {
            name : '_is_leaf',
            type : 'bool'
          }, {
            name : '_master',
            type : 'auto'
          }],
      evtRender : function(val) {
        /* For Ext 3.x: return Ext.util.Format.number(val,"0,000,000"); */
        s = val + '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(s))
          s = s.replace(rgx, '$1' + ',' + '$2');
        return s;
      },
      renderActual : function(val) {
        if (val == '')
          return '';
        if (val == '0')
          return 'No';
        return 'Yes';
      },
      buildUI : function() {
        var me = this;

        /*
         * Left panel
         */
        var selectors = {
          typeF : "Type",
          stateF : "State",
          authorF : "Author",
          wgF : "WG"
        };

        var textFields = {
          'idF' : {
            name : "Request ID(s)",
            type : "number"
          },
          'modF' : {
            name : "modF",
            fieldLabel : 'Show models only',
            type : "checkbox"
          }
        };

        var properties = [[]];
        var map = [["typeF", "typeF"], ["stateF", "stateF"], ["authorF", "authorF"], ["wgF", "wgF"]];

        me.leftPanel = Ext.create('Ext.dirac.utils.DiracBaseSelector', {
              scope : me,
              cmbSelectors : selectors,
              textFields : textFields,
              hasTimeSearchPanel : false,
              datamap : map,
              url : "ProductionRequestManager/getSelectionData",
              properties : properties
            });

        me.treeStore = Ext.create('LHCbDIRAC.ProductionRequestManager.classes.LHCbTreeStore', {
          model : 'LHCbDIRAC.BookkeepingBrowser.classes.BookkeepingTreeItemModel',
          remoteSort : true,
          pageSize : 10,
          fields : [{
                name : 'ID',
                type : 'auto'
              }, {
                name : 'reqName'
              }, {
                name : 'reqType'
              }, {
                name : 'reqState'
              }, {
                name : 'reqPrio'
              }, {
                name : 'reqAuthor'
              }, {
                name : 'reqWG'
              },

              {
                name : 'simCondID'
              }, {
                name : 'simDesc'
              }, {
                name : 'Generator'
              }, {
                name : 'G4settings'
              }, {
                name : 'MagneticField'
              }, {
                name : 'BeamEnergy'
              }, {
                name : 'Luminosity'
              }, {
                name : 'DetectorCond'
              }, {
                name : 'BeamCond'
              },

              {
                name : 'configName'
              }, {
                name : 'configVersion'
              }, {
                name : 'condType'
              }, {
                name : 'inProPass'
              }, {
                name : 'inFileType'
              }, {
                name : 'inProductionID'
              }, {
                name : 'inDataQualityFlag'
              }, {
                name : 'inTCKs'
              },

              {
                name : 'pID'
              }, {
                name : 'pDsc'
              }, {
                name : 'pAll'
              },

              {
                name : 'eventType'
              }, {
                name : 'eventText'
              }, {
                name : 'eventNumber'
              },

              {
                name : 'reqComment'
              }, {
                name : 'reqDesc'
              }, {
                name : 'reqInform'
              },

              {
                name : 'IsModel'
              }, {
                name : 'mcConfigVersion'
              },

              {
                name : 'eventBK'
              }, {
                name : 'EventNumberTotal'
              }, {
                name : 'eventBKTotal'
              }, {
                name : 'progress'
              }, {
                name : 'creationTime',
                type : 'date',
                dateFormat : 'Y-n-j h:i:s'
              }, {
                name : 'lastUpdateTime',
                type : 'date',
                dateFormat : 'Y-n-j h:i:s'
              },

              {
                name : 'TestState'
              }, {
                name : 'TestActual'
              },

              {
                name : '_parent',
                type : 'auto'
              }, {
                name : '_is_leaf',
                type : 'bool'
              }, {
                name : '_master',
                type : 'auto'
              }],
          scope : me,
          proxy : {
            type : 'ajax',
            url : GLOBAL.BASE_URL + me.applicationName + '/list'
          },
          root : {
            text : '.',
            // id : '/',
            expanded : false
          },
          scope : me,
          listeners : {
            beforeexpand : function(node, op) {
              console.log(node);
              if (node.get('ID') != "") {
                this.proxy.extraParams = {
                  "anode" : Ext.JSON.encode([node.get('ID')]),
                  "sort" : Ext.encode([{
                        "property" : "ID",
                        "direction" : "DESC"
                      }])
                };
              }
            }
          }
            /*
             * listeners : { beforeload : function(store, operation, eOpts) {
             * var me = this; store.proxy.extraParams = {"anode" :
             * Ext.JSON.encode([14]), "sort" : Ext.encode([{ "property" : "ID",
             * "direction" : "DESC" }])}; } }
             */

          });

        /*
         * var oProxy = Ext.create('Ext.dirac.utils.DiracAjaxProxy', { url :
         * GLOBAL.BASE_URL + me.applicationName + '/list' });
         * 
         * me.dataStore = Ext.create("Ext.dirac.utils.DiracJsonStore", {
         * autoLoad : false, proxy : oProxy, fields : me.dataFields, scope : me
         * });
         */

        var oColumns = {
          "checkBox" : {
            "dataIndex" : "IdCheckBox"
          },
          "Id" : {
            "dataIndex" : "ID",
            "properties" : {
              width : 50
            }
          },
          "Type" : {
            "dataIndex" : "reqType"
          },
          "State" : {
            "dataIndex" : "reqState"
          },
          "Priority" : {
            "dataIndex" : "reqPrio",
            "properties" : {
              width : 50
            }
          },
          "Name" : {
            "dataIndex" : "reqName",
            "id" : "Name"
          },
          "Sim/Run conditions" : {
            "dataIndex" : "simDesc",
            "properties" : {
              width : 200
            }
          },
          "Proc. pass" : {
            "dataIndex" : "pDsc",
            "properties" : {
              width : 200
            }
          },
          "Event type" : {
            "dataIndex" : "eventType"
          },
          "Events requested" : {
            "dataIndex" : "EventNumberTotal",
            renderer : me.evtRender
          },
          "Events in BK" : {
            "dataIndex" : "eventBKTotal",
            renderer : me.evtRender
          },
          "Progress (%)" : {
            "dataIndex" : "progress",
            "properties" : {
              align : 'right'
            }
          },
          "Created at" : {
            "dataIndex" : "creationTime",
            "properties" : {
              "hidden" : true
            },
            renderer : Ext.util.Format.dateRenderer('Y-m-d H:i')
          },
          "Last state update" : {
            "dataIndex" : "lastUpdateTime",
            "properties" : {
              hidden : true
            },
            renderer : Ext.util.Format.dateRenderer('Y-m-d H:i')
          },
          "Author" : {
            "dataIndex" : "reqAuthor",
            "properties" : {
              hidden : true
            }
          },
          "WG" : {
            "dataIndex" : "reqWG",
            "properties" : {
              "hidden" : true
            }
          },
          "Event type name" : {
            "dataIndex" : "eventText",
            "properties" : {
              "hidden" : true
            }
          },
          "Test" : {
            "dataIndex" : "TestState",
            "properties" : {
              "hidden" : true
            }
          },
          "Test is actual" : {
            "dataIndex" : "TestActual",
            "properties" : {
              hidden : true
            },
            renderer : me.renderActual
          }
        };
        var pagingToolbar = Ext.create("Ext.dirac.utils.DiracPagingToolbar", {
              // toolButtons : toolButtons,
              property : "StepAdministrator",
              store : me.dataStore,
              scope : me
            });
        var expanderTplBody = ['<b>Author:</b> {reqAuthor}<br/>', '<b>Beam:</b> {BeamCond} ', '<b>Beam energy:</b> {BeamEnergy} ', '<b>Generator:</b> {Generator} ', '<b>G4 settings:</b> {G4settings} ', '<b>Magnetic field:</b> {MagneticField} ',
            '<b>Detector:</b> {DetectorCond} ', '<b>Luminosity:</b> {Luminosity} <b>EventType:</b> {eventText} <br/>', '<b>Steps:</b> {pAll} ', '{htmlTestState}'];

        var expr = Ext.create("Ext.grid.plugin.RowExpander", {
              rowBodyTpl : expanderTplBody,
              enableCaching : false, // both are required for
              lazyRender : false
            });

        me.gridPanel = Ext.create("Ext.tree.Panel", {
              xtype : 'tree-grid',
              plugins : [{
                    ptype : "rowexpander",
                    rowBodyTpl : expanderTplBody
                  }],
              region : 'center',
              height : '600',
              header : false,
              viewConfig : {
                stripeRows : true,
                enableTextSelection : true
              },

              reserveScrollbar : true,

              useArrows : false,
              rootVisible : false,
              multiSelect : true,
              singleExpand : true,
              store : me.treeStore,
              bbar : {
                xtype : 'pagingtoolbar',
                pageSize : 10,
                store : me.treeStore,
                displayInfo : true
              },
              columns : [{
                    xtype : 'treecolumn', // this is so we know which column
                                          // will
                    // show the tree
                    text : 'ID',
                    // flex : 2,
                    sortable : true,
                    dataIndex : 'ID'
                  }, {
                    header : 'Type',
                    sortable : true,
                    dataIndex : 'reqType'
                  }, {
                    header : 'State',
                    sortable : true,
                    dataIndex : 'reqState'
                  }, {
                    header : 'Priority',
                    sortable : true,
                    dataIndex : 'reqPrio',
                    width : 50
                  }, {
                    header : 'Name',
                    sortable : true,
                    dataIndex : 'reqName'
                  }, {
                    header : 'Sim/Run conditions',
                    sortable : true,
                    dataIndex : 'simDesc',
                    width : 200
                  }, {
                    header : 'Proc. pass',
                    sortable : true,
                    dataIndex : 'pDsc',
                    width : 200
                  }, {
                    header : 'Event type',
                    sortable : true,
                    dataIndex : 'eventType'
                  }, {
                    header : 'Events requested',
                    dataIndex : 'EventNumberTotal',
                    renderer : me.evtRender,
                    align : 'right'
                  }, {
                    header : 'Events in BK',
                    dataIndex : 'eventBKTotal',
                    renderer : me.evtRender,
                    align : 'right'
                  }, {
                    header : 'Progress (%)',
                    dataIndex : 'progress',
                    align : 'right'
                  }, {
                    header : 'Created at',
                    sortable : true,
                    dataIndex : 'creationTime',
                    hidden : true,
                    renderer : Ext.util.Format.dateRenderer('Y-m-d H:i')
                  }, {
                    header : 'Last state update',
                    sortable : true,
                    dataIndex : 'lastUpdateTime',
                    hidden : true,
                    renderer : Ext.util.Format.dateRenderer('Y-m-d H:i')
                  }, {
                    header : 'Author',
                    dataIndex : 'reqAuthor',
                    hidden : true,
                    sortable : true
                  }, {
                    header : 'WG',
                    dataIndex : 'reqWG',
                    hidden : true,
                    sortable : true
                  }, {
                    header : 'Event type name',
                    dataIndex : 'eventText',
                    hidden : true
                  }, {
                    header : 'Test',
                    dataIndex : 'TestState',
                    hidden : true
                  }, {
                    header : 'Test is actual',
                    dataIndex : 'TestActual',
                    hidden : true,
                    renderer : me.renderActual
                  }]
            });

        me.leftPanel.setGrid(me.gridPanel);

        me.gridPanel.columns[1].setSortState("DESC");

        me.browserPanel = new Ext.create('Ext.panel.Panel', {
              layout : 'border',
              defaults : {
                collapsible : true,
                split : true
              },
              items : [me.leftPanel, me.gridPanel]
            });

        me.add([me.browserPanel]);

      }
    });
