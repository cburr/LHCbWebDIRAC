import json
import urllib
import re
import types
import cPickle
import tornado
from DIRAC import S_ERROR, S_OK, gConfig, gLogger
from DIRAC.ConfigurationSystem.Client.Helpers.Operations import Operations
from DIRAC.ConfigurationSystem.Client import PathFinder
from DIRAC.Core.DISET.RPCClient import RPCClient
from WebAppDIRAC.Lib.WebHandler import WebHandler, asyncGen
from DIRAC.Core.Utilities import Time

class ProductionRequestManagerHandler( WebHandler ):
  
  AUTH_PROPS = "authenticated"
  
  serviceFields = [ 'RequestID', 'HasSubrequest', 'ParentID', 'MasterID',
                    'RequestName', 'RequestType', 'RequestState',
                    'RequestPriority', 'RequestAuthor', 'RequestPDG', 'RequestWG',
                    'SimCondition', 'SimCondID',
                    'ProPath', 'ProID',
                    'EventType', 'NumberOfEvents', 'Comments', 'Description', 'Inform', 'IsModel',
                    'bk','bkTotal','rqTotal','crTime','upTime',
                    'TestState', 'TestActual' ]
  localFields  = [ 'ID', '_is_leaf', '_parent', '_master',
                   'reqName', 'reqType', 'reqState',
                   'reqPrio', 'reqAuthor', 'reqPDG', 'reqWG',
                   'simDesc', 'simCondID',
                   'pDsc', 'pID',
                   'eventType', 'eventNumber', 'reqComment', 'reqDesc', 'reqInform', 'IsModel',
                   'eventBK','eventBKTotal','EventNumberTotal',
                   'creationTime', 'lastUpdateTime',
                   'TestState','TestActual']

  simcondFields = [ 'Generator', 'MagneticField', 'BeamEnergy',
                    'Luminosity', 'DetectorCond', 'BeamCond',
                    'configName', 'configVersion', 'condType',
                    'inProPass', 'inFileType', 'inProductionID',
                    'inDataQualityFlag', 'G4settings', 'inTCKs' ]

  proStepFields = ['Step', 'Name', 'Pass', 'App', 'Ver', 'Opt', 'OptF',
                   'DDDb', 'CDb', 'DQT', 'EP',
                   'Vis', 'Use', 'IFT', 'OFT', 'Html' ]

  extraFields = ['mcConfigVersion']
  
  def __getArgument( self, argName, argDefValue ):
    # ToDo: I think it should be somewhere in DIRAC
    if argName not in self.request.arguments:
      return argDefValue
    if len( self.request.arguments[argName] ) > 0:
      return self.request.arguments[argName][0]
    return argDefValue
  
  def __getJsonArgument( self, argName, argDefValue ):
    # ToDo: I think it should be somewhere in DIRAC
    if argName not in self.request.arguments:
      return argDefValue
    if len( self.request.arguments[argName] ) > 0:
      return json.loads( self.request.arguments[argName][0] )
    return argDefValue
  
  def __2local(self,req):
    result = {}
    for x,y in zip(self.localFields,self.serviceFields):
      result[x] = req[y]
    result['_is_leaf'] = not result['_is_leaf']
    if req['bkTotal']!=None and req['rqTotal']!=None and req['rqTotal']:
      result['progress']=long(req['bkTotal'])*100/long(req['rqTotal'])
    else:
      result['progress']=None
    if req['SimCondDetail']:
      result.update(cPickle.loads(req['SimCondDetail']))
    if req['ProDetail']:
      result.update(cPickle.loads(req['ProDetail']))
    result['creationTime'] = str(result['creationTime'])
    result['lastUpdateTime'] = str(result['lastUpdateTime'])
    if req['Extra']:
      result.update(cPickle.loads(req['Extra']))
    for x in result:
      if x != '_parent' and result[x] == None:
        result[x] = ''
    return result;
  
  @asyncGen
  def web_getSelectionData( self ):
    callback = {}
    RPC = RPCClient( "ProductionManagement/ProductionRequest" )
    retVal = yield self.threadTask( RPC.getFilterOptions )
    if not retVal['OK']:
      self.finish( {"success":"false", "result":[], "total":0, "error":result["Message"]} )
    
    callback['typeF'] = [ [i] for i in retVal['Value']['Type']] 
    callback['stateF'] = [ [i] for i in retVal['Value']['State'] ]
    callback['authorF'] = [ [i] for i in retVal['Value']['State']]
    callback['wgF'] = [ [i] for i in retVal['Value']['WG']]

    self.write( callback )
  
  @asyncGen
  def web_list( self ):
    parent = self.request.arguments.get( 'anode', 0 )
    try:
      if parent != 0:
        jvalue =  json.loads(parent[-1])
        parent = long( jvalue[-1] )
        
    except Exception, e:
      self.finish( {"success":"false", "result":[], "total":0, "error": e} )

    try:
      offset = int( self.__getArgument( 'start', 0 ) )
      limit = int( self.__getArgument( 'limit', 0 ) )
      sortlist = self.__getJsonArgument( 'sort', [] )
      if len( sortlist ) > 0 :
        sortBy = str( sortlist[-1]['property'])
        sortBy = self.serviceFields[self.localFields.index( sortBy )]
        sortOrder  = str(sortlist[-1]['direction'])
      else:
        sortBy = "ID"
        sortOrder = "ASC"
                 
      filterOpts = {}
      for x, y in [( 'typeF', 'RequestType' ), ( 'stateF', 'RequestState' ),
                  ( 'authorF', 'RequestAuthor' ), ( 'idF', 'RequestID' ), ( 'modF', 'IsModel' ),
                  ( 'wgF', 'RequestWG' )]:
        
        val = self.request.arguments.get(x, "") 
        
        if val != '':
          val = list(json.loads(val[-1]))
          if len(val) > 0: 
            if not isinstance(val[0], bool):
              filterOpts[y] = ','.join(val)
            else:
              filterOpts[y] = val[-1]
                    
      if 'IsModel' in filterOpts and filterOpts['IsModel']:
        filterOpts['IsModel'] = 1
      else:
        filterOpts['IsModel'] = 0
    except Exception, e:
      self.finish( {"success":"false", "result":[], "total":0, "error":'Badly formatted list request: ' + str( e ) } )
      return  
    
    RPC = RPCClient( "ProductionManagement/ProductionRequest" )
    
    result = yield self.threadTask( RPC.getProductionRequestList, parent, sortBy, sortOrder, offset, limit, filterOpts );
    if not result['OK']:
      self.finish( {"success":"false", "result":[], "total":0, "error":result["Message"]} )
    rows = [self.__2local( x ) for x in result['Value']['Rows']]
    RPC = RPCClient( 'Bookkeeping/BookkeepingManager' )
    aet = yield self.threadTask( RPC.getAvailableEventTypes )
    if aet['OK']:
      etd = {}
      for et in aet['Value']:
        etd[str( et[0] )] = str( et[1] )
      for x in rows:
        if 'eventType' in x:
          x['eventText'] = etd.get( str( x['eventType'] ), '' )
    tosend = { "text": ".", "children":[]}
    for i in rows:
      i["leaf"] = i["_is_leaf"]
      #i['cls'] = "folder"
      tosend['children'] += [i]
    tosend['total'] = result['Value']['Total']
    timestamp = Time.dateTime().strftime( "%Y-%m-%d %H:%M [UTC]" )
    result = tornado.escape.json_encode(tosend)
    self.finish(result)
    #self.finish({"success":"true", "result":tosend, "total":result['Value']['Total'], "date":timestamp })
  
  @asyncGen 
  def web_test(self):
    s = """{"text":".","children": [
    {
        task:'Project: Shopping',
        duration:13.25,
        user:'Tommy Maintz',
        iconCls:'task-folder',
        expanded: true,
        children:[{
            task:'Housewares',
            duration:1.25,
            user:'Tommy Maintz',
            iconCls:'task-folder',
            children:[{
                task:'Kitchen supplies',
                duration:0.25,
                user:'Tommy Maintz',
                leaf:true,
                iconCls:'task'
            },{
                task:'Groceries',
                duration:.4,
                user:'Tommy Maintz',
                leaf:true,
                iconCls:'task'
            },{
                task:'Cleaning supplies',
                duration:.4,
                user:'Tommy Maintz',
                leaf:true,
                iconCls:'task'
            },{
                task: 'Office supplies',
                duration: .2,
                user: 'Tommy Maintz',
                leaf: true,
                iconCls: 'task'
            }]
        }, {
            task:'Remodeling',
            duration:12,
            user:'Tommy Maintz',
            iconCls:'task-folder',
            expanded: true,
            children:[{
                task:'Retile kitchen',
                duration:6.5,
                user:'Tommy Maintz',
                leaf:true,
                iconCls:'task'
            },{
                task:'Paint bedroom',
                duration: 2.75,
                user:'Tommy Maintz',
                iconCls:'task-folder',
                children: [{
                    task: 'Ceiling',
                    duration: 1.25,
                    user: 'Tommy Maintz',
                    iconCls: 'task',
                    leaf: true
                }, {
                    task: 'Walls',
                    duration: 1.5,
                    user: 'Tommy Maintz',
                    iconCls: 'task',
                    leaf: true
                }]
            },{
                task:'Decorate living room',
                duration:2.75,
                user:'Tommy Maintz',
                leaf:true,
                iconCls:'task'
            },{
                task: 'Fix lights',
                duration: .75,
                user: 'Tommy Maintz',
                leaf: true,
                iconCls: 'task'
            }, {
                task: 'Reattach screen door',
                duration: 2,
                user: 'Tommy Maintz',
                leaf: true,
                iconCls: 'task'
            }]
        }]
    },{
        task:'Project: Testing',
        duration:2,
        user:'Core Team',
        iconCls:'task-folder',
        children:[{
            task: 'Mac OSX',
            duration: 0.75,
            user: 'Tommy Maintz',
            iconCls: 'task-folder',
            children: [{
                task: 'FireFox',
                duration: 0.25,
                user: 'Tommy Maintz',
                iconCls: 'task',
                leaf: true
            }, {
                task: 'Safari',
                duration: 0.25,
                user: 'Tommy Maintz',
                iconCls: 'task',
                leaf: true
            }, {
                task: 'Chrome',
                duration: 0.25,
                user: 'Tommy Maintz',
                iconCls: 'task',
                leaf: true
            }]
        },{
            task: 'Windows',
            duration: 3.75,
            user: 'Darrell Meyer',
            iconCls: 'task-folder',
            children: [{
                task: 'FireFox',
                duration: 0.25,
                user: 'Darrell Meyer',
                iconCls: 'task',
                leaf: true
            }, {
                task: 'Safari',
                duration: 0.25,
                user: 'Darrell Meyer',
                iconCls: 'task',
                leaf: true
            }, {
                task: 'Chrome',
                duration: 0.25,
                user: 'Darrell Meyer',
                iconCls: 'task',
                leaf: true
            },{
                task: 'Internet Exploder',
                duration: 3,
                user: 'Darrell Meyer',
                iconCls: 'task',
                leaf: true
            }]
        },{
            task: 'Linux',
            duration: 0.5,
            user: 'Aaron Conran',
            iconCls: 'task-folder',
            children: [{
                task: 'FireFox',
                duration: 0.25,
                user: 'Aaron Conran',
                iconCls: 'task',
                leaf: true
            }, {
                task: 'Chrome',
                duration: 0.25,
                user: 'Aaron Conran',
                iconCls: 'task',
                leaf: true
            }]
        }]
    }
]}"""
    self.write(s)