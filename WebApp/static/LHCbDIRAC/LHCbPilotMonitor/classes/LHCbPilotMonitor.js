/**
 * It is used to open the LHCb specific JobMonitor application
 */
Ext.define('LHCbDIRAC.LHCbPilotMonitor.classes.LHCbPilotMonitor', {
      extend : 'DIRAC.PilotMonitor.classes.PilotMonitor',
      initComponent : function() {

        var me = this;
        me.callParent();
        me.launcher.title = "LHCb Pilot Monitor";
        me.applicationsToOpen = {
          'JobMonitor' : 'LHCbDIRAC.LHCbJobMonitor.classes.LHCbJobMonitor'
        }

      }
    });