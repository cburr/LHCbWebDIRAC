/**
 * 
 */
Ext.define('LHCbDIRAC.BookkeepingBrowser.classes.BookkeepingTreeItemModel', {
   extend : 'Ext.data.Model',
   fields : ['text', 'selection', 'fullpath', 'level'],
   alias : 'widget.bkktreemenumodel'
  });
